cmake_minimum_required(VERSION 3.10)

project(Project21)

set(CMAKE_CXX_STANDARD 20)

option(TESTS_ENABLED "Enable additional tests" ON)

set(B21_TRACING_TYPE "B21_TRACING_TYPE_TRACY")

add_subdirectory(third_party)

add_subdirectory(src)

enable_testing()
add_test(NAME deck_test COMMAND deck_test)
