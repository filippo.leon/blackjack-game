#include <algorithm>
#include <future>
#include <iostream>
#include <memory>
#include <string>

#include "Application.hpp"
#include "dealer.hpp"
#include "deck/RiggedDeck.hpp"
#include "game.hpp"
#include "player.hpp"
#include "profiling/profiler.hpp"
#include "ruleset.hpp"
#include "statistics/MeanAndVariance.hpp"
#include "strategy/OptimalPlayerStrategy.hpp"
#include "types.hpp"
#include "util.hpp"
#include "util/BlockTImer.hpp"
#include "util/format.hpp"


int main(int argc, char **argv) {
    int nSims = 10;
    int nRounds = 1000000;
    if (argc > 1) {
        nSims = std::stoi(std::string(argv[1]));
    }
    if (argc > 2) {
        nRounds = std::stoi(std::string(argv[2]));
    }
    int outputLevel = 5;
    if (argc > 3) {
        outputLevel = std::stoi(std::string(argv[3]));
    }

    std::cout << "Usage: expecation.exe [nSims] [nRounds] [outputLevel]" << std::endl;
    std::cout << "Starting simulation with " << nSims << " parallel simulations of " << nRounds
              << " each. (Each bet has a value of 10.) Please wait..." << std::endl;

    Application app{};
    app.Init();

    SetOutputLevel(outputLevel);

    auto makeSim = [nRounds]() {
        BlockTimer tm("Took:");

        RuleSet ruleSet{};

        // auto deck = std::make_unique<RiggedDeck>(5, std::vector<Card>{Card::A, Card::A, Card::A});
        auto deck = std::make_unique<Deck>(5);
        deck->Shuffle();

        Player player(0, std::make_unique<OptimalPlayerStrategy>());

        Dealer dealer(std::move(deck), ruleSet);

        for (int i = 0; i < nRounds; ++i) {
            B21_TRACE_SCOPE();
            const int completed = (i + 1) * 100 / nRounds;
            const int missing = 100 - completed;
            Output<5>("[{} / {}] [{:=>{}}{: >{}}]\r", i, nRounds, "", completed, "", missing);
            PlayRound(dealer, player, ruleSet);
        }
        Output<5>("\n");

        double expectation = static_cast<double>(player.GetFunds()) / static_cast<double>(nRounds);
        Output<5>("Result for {} rounds: {} (exp: {}).\n", nRounds, player.GetFunds(), expectation);

        return expectation;
    };

    struct Result {
        std::future<double> future;
        std::thread thread;
    };

    std::vector<Result> expectations;

    for (int i = 0; i < nSims; ++i) {
        auto task = std::packaged_task<double()>(makeSim);
        auto fut = task.get_future();
        expectations.push_back(Result{std::move(fut), std::thread(std::move(task))});
    }

    MeanAndVariance<double> rmse;
    for (auto &exp : expectations) {
        exp.thread.join();
        rmse.Add(exp.future.get());
        std::cout << "Done" << std::endl;
    }

    auto meanVarSampleVar = rmse.Get();
    std::cout << "Mean: " << std::get<0>(meanVarSampleVar) << ", variance: " << std::get<1>(meanVarSampleVar) << " "
              << ", sample variance: " << std::get<2>(meanVarSampleVar) << " " << std::endl;
}
