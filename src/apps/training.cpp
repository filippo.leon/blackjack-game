#include <algorithm>
#include <iostream>
#include <memory>

#include "Application.hpp"
#include "dealer.hpp"
#include "deck/Deck.hpp"
#include "game.hpp"
#include "player.hpp"
#include "strategy/TrainingPlayerStrategy.hpp"
#include "types.hpp"
#include "util.hpp"


int main() {
    Application app{};
    app.Init();

    RuleSet ruleSet{};

    auto deck = std::make_unique<Deck>(6);

    Player player(1000, std::make_unique<TrainingPlayerStrategy>());

    Dealer dealer(std::move(deck), ruleSet);

    while (player.GetFunds() > 0) {
        PlayRound(dealer, player, ruleSet);
    }
}
