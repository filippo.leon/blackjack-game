#pragma once

#include <array>

#include "strategy/IPlayerStrategy.hpp"
#include "types.hpp"
#include "util.hpp"
#include "util/debug.hpp"
#include "util/format.hpp"

class Player {
public:
    explicit Player(int initialFunds, std::unique_ptr<IPlayerStrategy> strategy)
        : m_funds(initialFunds), m_strategy(std::move(strategy)) {
        m_handAndBets.resize(1);
    }

    void AddFunds(int amount) { m_funds += amount; }

    void RemoveFunds(int amount) { m_funds -= amount; }

    void AddBet(int amount, int hand) {
        m_funds -= amount;
        Output<0>("Player bets {} to hand {} (remaining funds {})\n", amount, hand, m_funds);
        m_handAndBets[hand].bet += amount;
    }

    int GetBet(int hand) const { return m_handAndBets[hand].bet; }

    int Split(int hand) {
        ASSERT(hand >= 0 && hand < m_handAndBets.size());
        ASSERT(m_handAndBets[hand].hand.size() == 2);
        m_handAndBets.push_back(m_handAndBets[hand]);
        int newHand = static_cast<int>(m_handAndBets.size()) - 1;
        m_funds -= m_handAndBets[hand].bet;

        m_handAndBets[hand].hand.resize(1);
        m_handAndBets[newHand].hand.resize(1);

        return newHand;
    }

    void Surrender(int hand) {
        int halfBet = m_handAndBets[hand].bet / 2;
        m_funds += halfBet;
        m_handAndBets[hand].bet = halfBet;
        m_handAndBets[hand].surrendered = true;
    }

    IPlayerStrategy &GetStrategy() { return *m_strategy; }

    void Deal(Card card, int hand) { m_handAndBets[hand].hand.push_back(card); }

    int GetNumHands() const { return static_cast<int>(m_handAndBets.size()); }

    const Hand &GetHand(int hand) const { return m_handAndBets[hand].hand; }

    bool IsBust(int hand) const {
        bool soft = false;
        const int handValue = GetValue(m_handAndBets[hand].hand, &soft);
        ASSERT(!soft || handValue <= BlackJackValue);
        return handValue > BlackJackValue || m_handAndBets[hand].surrendered;
    }

    void ResolveBet(int hand, Status status) {
        switch (status) {
            case Status::Win:
                m_funds += 2 * m_handAndBets[hand].bet;
                Output<0>("{}, gain {} for hand {} (remaining funds: {})\n", status, m_handAndBets[hand].bet, hand,
                          m_funds);
                break;
            case Status::Loss:
                Output<0>("{}, lose {} for hand {} (remaining funds: {})\n", status, m_handAndBets[hand].bet, hand,
                          m_funds);
                break;
            case Status::Surrendered:
                m_funds += m_handAndBets[hand].bet / 2;
                Output<0>("{}, surrendered {} for hand {} (remaining funds: {})\n", status, m_handAndBets[hand].bet,
                          hand, m_funds);
                break;
            case Status::Tie:
                m_funds += m_handAndBets[hand].bet;
                Output<0>("{}, no change for hand {} (remaining funds: {})\n", status, hand, m_funds);
                break;
            case Status::BlackJack: {
                const int blackJackBet = m_handAndBets[hand].bet * 3 / 2; // TODO: blackjack payout
                m_funds += m_handAndBets[hand].bet + blackJackBet;

                Output<0>("{}, gain {} for hand {} (remaining funds: {})\n", status, blackJackBet, hand, m_funds);
                break;
            }
            case Status::Invalid: ASSERT(false && "Invalid status");
        }
        m_handAndBets[hand].bet = 0;
    }

    int GetFunds() const { return m_funds; }

    void Clear() {
        m_handAndBets.clear();
        m_handAndBets.resize(1);
    };

private:
    int m_funds = 0;

    struct HandAndBet {
        Hand hand;

        int bet = 0;

        bool surrendered = false;
    };

    std::unique_ptr<IPlayerStrategy> m_strategy;

    std::vector<HandAndBet> m_handAndBets;
};
