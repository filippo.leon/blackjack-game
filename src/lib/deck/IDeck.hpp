#pragma once

#include "types.hpp"

class IDeck {
public:
    virtual void Reset() = 0;

    virtual void Shuffle() = 0;

    virtual Card Draw() = 0;
};
