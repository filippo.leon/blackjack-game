#pragma once

#include "deck/Deck.hpp"
#include "deck/IDeck.hpp"
#include "util/debug.hpp"

class RiggedDeck : public Deck {
public:
    RiggedDeck(int numDecks, const std::vector<Card> &initialCards) : Deck(numDecks) {
        Deck::Reset();

        m_initialShuffle = static_cast<int>(initialCards.size());
        for (int i = 0; i < m_initialShuffle; ++i) {
            Card c = initialCards[i];
            auto it = std::find(m_deck.begin() + i, m_deck.end(), c);
            ASSERT(it != m_deck.end());
            std::swap(*it, *(m_deck.begin() + i));
        }

        m_riggedDeck = m_deck;
        Shuffle();
    }

    void Reset() override { m_deck = m_riggedDeck; }

    void Shuffle() override { ShuffleImpl(m_initialShuffle); }

private:
    std::deque<Card> m_riggedDeck;

    int m_initialShuffle = 0;
};
