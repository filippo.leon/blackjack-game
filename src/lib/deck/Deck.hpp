#pragma once

#include <algorithm>
#include <array>
#include <deque>
#include <numeric>
#include <random>
#include <vector>

#include "deck/IDeck.hpp"
#include "profiling/profiler.hpp"
#include "random/well512a.hpp"
#include "ruleset.hpp"
#include "types.hpp"

class Deck : public IDeck {
public:
    explicit Deck(int size) : m_size(size) {
        std::random_device rd;
        std::mt19937 g(rd());

        std::array<unsigned int, 16> init;
        std::uniform_int_distribution<int> dist{};
        std::generate(init.begin(), init.end(), [&]() { return dist(g); });
        m_random = Well512a(init);

        Reset();
        Shuffle();
    }

    void Reset() override {
        B21_TRACE_SCOPE();
        m_deck.clear();

        m_deck.resize(m_size * 13 * 4);
        for (int i = 0; i < m_size * 4; ++i) {
            for (int j = 0; j < 13; ++j) {
                m_deck[13 * i + j] = static_cast<Card>(j + 1);
            }
        }
    }

    void Shuffle() override {
        B21_TRACE_SCOPE();
        ShuffleImpl();
    }

    Card Draw() override {
        B21_TRACE_SCOPE();
        if (m_deck.empty()) {
            Reset();
            Shuffle();
        }
        Card card = m_deck.front();
        m_deck.pop_front();
        return card;
    }

protected:
    void ShuffleImpl(int startElem = 0) { std::shuffle(m_deck.begin() + startElem, m_deck.end(), m_random); }

    int m_size = 0;

    std::deque<Card> m_deck;

    Well512a m_random;
};
