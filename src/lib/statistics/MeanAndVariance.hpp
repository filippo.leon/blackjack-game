#pragma once

#include <exception>
#include <tuple>

template<class T_>
class MeanAndVariance {
public:
    void Add(const T_ &value) {

        m_count += 1;
        auto delta = value - m_mean;
        m_mean += delta / m_count;
        auto delta2 = value - m_mean;
        m_m2 += delta * delta2;
    }

    std::tuple<T_, T_, T_> Get() const {
        if (m_count < 2) {
            throw std::exception{}; // TODO
        }

        return {m_mean, m_m2 / m_count, m_m2 / (m_count + 1)};
    }

private:
    int m_count = 0;
    T_ m_mean{};
    T_ m_m2{};
};
