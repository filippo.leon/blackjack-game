#pragma once

#define USE_UTF_TERMINAL 1
#define USE_ANSI_COLORS 1

constexpr bool utf8terminal = true;
constexpr bool ansiColors = true;

constexpr int outputLevel = 0;

enum class LogLevel { Verbose, Info, Warning, Error };

static constexpr LogLevel logLevel = LogLevel::Verbose;

enum class BuildOs {
    Windows,
    Mac,
    Linux
}

#ifdef _WIN32
static constexpr buildOs = BuildOs::Windows;
#endif
