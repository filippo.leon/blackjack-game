#include "game.hpp"

#include <iostream>

#include "dealer.hpp"
#include "player.hpp"
#include "profiling/profiler.hpp"
#include "ruleset.hpp"

void PlayRound(Dealer &dealer, Player &player, const RuleSet &ruleSet) {
    B21_TRACE_SCOPE();

    Output<0>("\n\n\n\n\n================= ");
    Output<0>("New round started");
    Output<0>(" =================\n\n\n\n\n");

    {
        const int bet = player.GetStrategy().GetBet();
        player.AddBet(bet, 0);
    }

    if (dealer.DealInitialHand(player)) {
        for (int hand = 0; hand < player.GetNumHands(); ++hand) {
            while (true) {
                B21_NAMED_TRACE_SCOPE("MainLoop");
                Action action = player.GetStrategy().Decide(
                        dealer.GetHand(), player.GetHand(hand), player.GetNumHands(),
                        GetPossibleActions(ruleSet, player.GetHand(hand), player.GetNumHands(), false));

                dealer.PerformAction(player, action, hand);

                const Hand &h = player.GetHand(hand);
                // const int value = GetValue(h);
                Output<0>("Dealer hand {}\n", dealer.GetHand());
                Output<0>("Player hand {}: {}\n", hand, h);

                if (player.IsBust(hand) || action == Action::Stay || action == Action::Double ||
                    action == Action::Surrender) {
                    break;
                }
            }
        }

        dealer.Finalize();

        dealer.Payout(player);
    }

    dealer.Clear();

    player.Clear();

    if (ruleSet.shuffleAfterEachRound) {
        dealer.GetDeck().Reset();
        dealer.GetDeck().Shuffle();
    }
}
