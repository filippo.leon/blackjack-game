#pragma once

#include <cassert>
#include <iostream>
#include <memory>

#include "deck/IDeck.hpp"
#include "player.hpp"
#include "profiling/profiler.hpp"
#include "rules.hpp"
#include "ruleset.hpp"
#include "types.hpp"
#include "util.hpp"
#include "util/debug.hpp"
#include "util/format.hpp"

class IDeck;
struct RuleSet;

class Dealer {
public:
    explicit Dealer(std::unique_ptr<IDeck> deck, const RuleSet &ruleSet)
        : m_deck(std::move(deck)), m_ruleSet(ruleSet) {}

    const Hand &GetHand() const { return m_hand; }

    IDeck &GetDeck() { return *m_deck; }

    bool DealInitialHand(Player &player) {
        player.Deal(m_deck->Draw(), 0);
        m_hand.push_back(m_deck->Draw());
        player.Deal(m_deck->Draw(), 0);

        m_holeCard = m_deck->Draw();

        Hand hand2 = m_hand;
        hand2.push_back(m_holeCard);

        const Hand &playerHand = player.GetHand(0);
        Output<0>("Dealer initial hand: {} (hole card: {}): \n", m_hand, m_holeCard, hand2);
        Output<0>("Player initial hand: {}\n", playerHand);

        auto dealerCompleteHandValue = GetValue(hand2);
        if (m_ruleSet.earlySurrender || dealerCompleteHandValue != BlackJackValue) {

            Action action =
                    player.GetStrategy().Decide(GetHand(), player.GetHand(0), player.GetNumHands(),
                                                GetPossibleActions(m_ruleSet, playerHand, player.GetNumHands(), true));
            if (action == Action::Surrender) {
                player.ResolveBet(0, Status::Surrendered);
                return false; // autoresolve
            }
        }

        if (dealerCompleteHandValue == BlackJackValue) {
            Output<0>("Dealer has blackjack!");
            if (GetValue(playerHand) == BlackJackValue) {
                Output<0>("Player has blackjack!");
                player.ResolveBet(0, Status::Win);
            }
            player.ResolveBet(0, Status::Loss);

            return false; // autoresolve
        }
        if (GetValue(playerHand) == BlackJackValue) {
            Output<0>("Player has blackjack!");
            player.ResolveBet(0, Status::BlackJack);
            return false; // autoresolve
        }

        return true;
    }

    void PerformAction(Player &player, Action action, int hand) {
        switch (action) {
            case Action::Invalid: ASSERT(false && "Invalid action");
            case Action::Hit: player.Deal(m_deck->Draw(), hand); break;
            case Action::Split: {
                const int newHand = player.Split(hand);
                player.Deal(m_deck->Draw(), hand);
                player.Deal(m_deck->Draw(), newHand);
                break;
            }
            case Action::Double:
                player.AddBet(player.GetBet(hand), hand);
                player.Deal(m_deck->Draw(), hand);
                break;
            case Action::Surrender:
                ASSERT(false &&
                       "Cannot surrender at this stage"); // player.Surrender(hand); // Cannot surrender this late
                break;
            case Action::Stay: break;
        }
        Output<0>("Player chooses '{}' for hand {}\n", action, hand);
    }

    bool ShouldStop(int value, bool soft) const {
        if (value == 17 && soft) {
            return !m_ruleSet.dealerHitSoft17;
        }
        return value >= 17;
    }

    void Finalize() {
        m_hand.push_back(m_holeCard);

        while (true) {
            bool soft = false;
            const int value = GetValue(m_hand, &soft);
            if (ShouldStop(value, soft)) {
                break;
            }
            m_hand.push_back(m_deck->Draw());
        }

        Output<0>("Dealer final hand: {}.\n", m_hand);
    }

    void Payout(Player &player) {
        B21_TRACE_SCOPE();
        const int dealerValue = GetValue(m_hand);
        for (int hand = 0; hand < player.GetNumHands(); ++hand) {
            if (player.IsBust(hand)) {
                player.ResolveBet(hand, Status::Loss);
                continue;
            }
            if (dealerValue > BlackJackValue) {
                player.ResolveBet(hand, Status::Win);
                continue;
            }
            const int playerValue = GetValue(player.GetHand(hand));
            if (playerValue > dealerValue) {
                player.ResolveBet(hand, Status::Win);
            } else if (playerValue == dealerValue) {
                player.ResolveBet(hand, Status::Tie);
            } else {
                player.ResolveBet(hand, Status::Loss);
            }
        }
        player.GetStrategy().Finalize();
    }

    void Clear() {
        B21_TRACE_SCOPE();
        m_hand.clear();
        m_holeCard = Card::Invalid;
    }

private:
    std::unique_ptr<IDeck> m_deck;

    const RuleSet &m_ruleSet;

    Hand m_hand;

    Card m_holeCard = Card::Invalid;
};
