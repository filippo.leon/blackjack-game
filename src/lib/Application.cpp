#include "Application.hpp"

#include <cstdio>
#include <iostream>

#include "config.hpp"

#ifdef _WIN32
#    include "Windows.h"
#endif

namespace {

bool g_hasAnsiColors = false;
bool g_hasUtfTerminal = false;

} // namespace

bool HasAnsiColors() {
#if USE_ANSI_COLORS
    return g_hasAnsiColors;
#else
    return false;
#endif
}

bool HasUTF8Terminal() {
#if USE_UTF_TERMINAL
    return true;
#else
    return false;
#endif
}

void Application::Init() {
    if constexpr (buildOs == BuildOs::Windows) {
        SetConsoleOutputCP(CP_UTF8);

        setvbuf(stdout, nullptr, _IOFBF, 1000);

        HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

        DWORD dwMode;
        GetConsoleMode(hOutput, &dwMode);
        dwMode |= ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING;
        if (SetConsoleMode(hOutput, dwMode) == 0) {
            std::cout << "Error setting console color." << std::endl;
        } else {
            g_hasAnsiColors = true;
        }
    }
}
