#pragma once

#include <cassert>
#include <iostream>

#include <fmt/format.h>

#include "Application.hpp"
#include "types.hpp"
#include "util/debug.hpp"
#include "util/terminal.hpp"

inline int GetValue(const Card &card) {
    switch (card) {
        case Card::A: return 1;
        case Card::J:
        case Card::Q:
        case Card::K: return 10;
        default: return static_cast<int>(card);
    }
}

inline int GetValue(const Hand &hand, bool *soft = nullptr) {
    int value = 0;
    int numAces = 0;
    for (const Card &card : hand) {
        if (card == Card::A) {
            numAces++;
        } else {
            value += GetValue(card);
        }
    }
    int valueWith11Ace = value + 11 + (numAces - 1);
    if (numAces > 0 && valueWith11Ace <= BlackJackValue) {
        if (soft != nullptr) {
            *soft = true;
        }
        return valueWith11Ace;
    }
    if (soft != nullptr) {
        *soft = false;
    }
    return value + numAces;
}

inline std::string ToString(Status action) { // NOLINT
    switch (action) {
        case Status::Invalid: ASSERT(false && "Invalid status");
        case Status::Tie: return yellow + "Tie" + reset;
        case Status::Loss: return red + "Loss" + reset;
        case Status::Win: return green + "Win" + reset;
        case Status::BlackJack: return blue + "BlackJack" + reset;
        case Status::Surrendered: return magenta + "Surrender" + reset;
    }

    assert(false && "Invalid status");
    return "";
}

inline std::string ToString(Action action) { // NOLINT
    switch (action) {
        case Action::Invalid: ASSERT(false && "Invalid action");
        case Action::Stay: return yellow + "Stay" + reset;
        case Action::Hit: return red + "Hit" + reset;
        case Action::Split: return green + "Split" + reset;
        case Action::Double: return blue + "Double" + reset;
        case Action::Surrender: return magenta + "Surrender" + reset;
    }

    ASSERT(false && "Invalid action");
    return "";
}

inline std::string ToString(Card card) { // NOLINT
    if (HasAnsiColors()) {
        if (HasUTF8Terminal()) {
            switch (card) {
                case Card::Invalid: ASSERT(false && "Invalid card");
                case Card::A: return red + "A" + " 🂡" + reset;
                case Card::N2: return green + "2" + " 🂢" + reset;
                case Card::N3: return green + "3" + " 🂣" + reset;
                case Card::N4: return green + "4" + " 🂤" + reset;
                case Card::N5: return green + "5" + " 🂥" + reset;
                case Card::N6: return green + "6" + " 🂦" + reset;
                case Card::N7: return green + "7" + " 🂧" + reset;
                case Card::N8: return green + "8" + " 🂨" + reset;
                case Card::N9: return green + "9" + " 🂩" + reset;
                case Card::N10: return green + "10" + " 🂪" + reset;
                case Card::J: return blue + "J" + " 🂬" + reset;
                case Card::Q: return blue + "Q" + " 🂭" + reset;
                case Card::K: return blue + "K" + " 🂮" + reset;
            }
        } else {
            switch (card) {
                case Card::Invalid: ASSERT(false && "Invalid card");
                case Card::A: return red + "A" + reset;
                case Card::N2: return green + "2" + reset;
                case Card::N3: return green + "3" + reset;
                case Card::N4: return green + "4" + reset;
                case Card::N5: return green + "5" + reset;
                case Card::N6: return green + "6" + reset;
                case Card::N7: return green + "7" + reset;
                case Card::N8: return green + "8" + reset;
                case Card::N9: return green + "9" + reset;
                case Card::N10: return green + "10" + reset;
                case Card::J: return blue + "J" + reset;
                case Card::Q: return blue + "Q" + reset;
                case Card::K: return blue + "K" + reset;
            }
        }
    } else {
        if (HasUTF8Terminal()) {
            switch (card) {
                case Card::Invalid: ASSERT(false && "Invalid card");
                case Card::A: return "A 🂡";
                case Card::N2: return "2 🂢";
                case Card::N3: return "3 🂣";
                case Card::N4: return "4 🂤";
                case Card::N5: return "5 🂥";
                case Card::N6: return "6 🂦";
                case Card::N7: return "7 🂧";
                case Card::N8: return "8 🂨";
                case Card::N9: return "9 🂩";
                case Card::N10: return "10 🂪";
                case Card::J: return "J 🂬";
                case Card::Q: return "Q 🂭";
                case Card::K: return "K 🂮";
            }
        } else {
            switch (card) {
                case Card::Invalid: ASSERT(false && "Invalid card");
                case Card::A: return red + "A";
                case Card::N2: return "2";
                case Card::N3: return "3";
                case Card::N4: return "4";
                case Card::N5: return "5";
                case Card::N6: return "6";
                case Card::N7: return "7";
                case Card::N8: return "8";
                case Card::N9: return "9";
                case Card::N10: return "10";
                case Card::J: return "J";
                case Card::Q: return "Q";
                case Card::K: return "K";
            }
        }
    }

    ASSERT(false && "Invalid card");
    return "";
}

inline std::string ToString(const Hand &hand) {
    std::string str;
    for (const Card &card : hand) {
        str += ToString(card) + " ";
    }
    bool soft = false;
    const int value = GetValue(hand, &soft);
    str += std::string("(value is ") + (soft ? "S" : "") + std::to_string(value) + ")";
    return str;
}

inline std::ostream &operator<<(std::ostream &o, const Hand &hand) {
    o << ToString(hand);
    return o;
}

inline std::ostream &operator<<(std::ostream &o, const Card &card) {
    o << ToString(card);
    return o;
}

inline std::ostream &operator<<(std::ostream &o, const Status &status) {
    o << ToString(status);
    return o;
}

inline std::ostream &operator<<(std::ostream &o, const Action &action) {
    o << ToString(action);
    return o;
}

template<>
struct fmt::formatter<Status> : formatter<std::string_view> {
    template<typename FormatContext>
    auto format(Status c, FormatContext &ctx) {
        return formatter<string_view>::format(ToString(c), ctx);
    }
};

template<>
struct fmt::formatter<Action> : formatter<std::string_view> {
    template<typename FormatContext>
    auto format(Action c, FormatContext &ctx) {
        return formatter<string_view>::format(ToString(c), ctx);
    }
};

template<>
struct fmt::formatter<Card> : formatter<std::string_view> {
    template<typename FormatContext>
    auto format(Card c, FormatContext &ctx) {
        return formatter<string_view>::format(ToString(c), ctx);
    }
};

template<>
struct fmt::formatter<Hand> : formatter<std::string_view> {
    template<typename FormatContext>
    auto format(const Hand &c, FormatContext &ctx) {
        return formatter<string_view>::format(ToString(c), ctx);
    }
};
