#pragma once

struct RuleSet {
    int numDecks = 6; // TODO

    bool shuffleAfterEachRound = true; // TODO

    bool dealerHitSoft17 = true; // TODO

    bool earlySurrender = true;

    bool surrenderAces = false;

    int maxNumSplits = 4; // TODO
};
