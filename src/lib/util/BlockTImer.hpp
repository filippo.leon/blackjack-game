#pragma once

#include <chrono>
#include <string>

#include "util/format.hpp"


class BlockTimer {
public:
    /* implicit */ BlockTimer(std::string desc) // NOLINT
        : m_desc(std::move(desc)), m_start(std::chrono::high_resolution_clock::now()) {}

    ~BlockTimer() {
        auto now = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_start).count();
        Output<1>("{} {} ms", m_desc, duration);
    }

private:
    std::string m_desc;

    std::chrono::high_resolution_clock::time_point m_start;
};
