#pragma once

#include <iostream>

#include "Windows.h" // TODO
#include "util/terminal.hpp"

#define ASSERT(expr) static_cast<bool>((expr)) ? 0 : detail::AssertImpl((#expr), __FILE__, __LINE__, __FUNCTION__)

namespace detail {

inline int AssertImpl(const std::string &str, const std::string &file, const int line, const std::string &function) {
    std::cout << red << "ERROR: Assertion: '" << str << "' failed at " << file << ":" << line << " in function "
              << function << "." << reset << std::endl;
    DebugBreak(); // TODO
    std::terminate();
    return 0;
}

} // namespace detail
