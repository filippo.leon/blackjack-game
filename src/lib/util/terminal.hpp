#pragma once

#include <string>

#include "config.hpp"

inline const std::string reset = "\033[0m";

inline const std::string bright = "\033[1m";
inline const std::string underline = "\033[4m";
inline const std::string inverse = "\033[7m";
inline const std::string bright_off = "\033[21m";
inline const std::string underline_off = "\033[24m";
inline const std::string inverse_off = "\033[27m";

inline const std::string black = "\033[30m";
inline const std::string red = "\033[31m";
inline const std::string green = "\033[32m";
inline const std::string yellow = "\033[33m";
inline const std::string blue = "\033[34m";
inline const std::string magenta = "\033[35m";
inline const std::string cyan = "\033[36m";
inline const std::string white = "\033[37m";

inline const std::string black_bg = "\033[40m";
inline const std::string red_bg = "\033[41m";
inline const std::string green_bg = "\033[42m";
inline const std::string yellow_bg = "\033[43m";
inline const std::string blue_bg = "\033[44m";
inline const std::string magenta_bg = "\033[45m";
inline const std::string cyan_bg = "\033[46m";
inline const std::string white_bg = "\033[47m";
