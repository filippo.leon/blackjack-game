#pragma once

#include <iostream>
#include <utility>

#include <fmt/format.h>

#include "config.hpp"

inline int g_outputLevel = 0;

inline void SetOutputLevel(int level) {
    g_outputLevel = level;
}

inline int GetOutputLevel() {
    return g_outputLevel;
}

template<class... Args_>
std::string Format(const std::string &fmtStr, Args_ &&...args) {
    return fmt::format(fmt::runtime(fmtStr), std::forward<Args_>(args)...);
}

template<unsigned int Level_, class... Args_>
void Output(const std::string &fmtStr, Args_ &&...args) {
    if (Level_ >= GetOutputLevel()) {
        std::cout << Format(fmtStr, std::forward<Args_>(args)...) << std::flush;
    }
}
