#pragma once

#include <iostream>

#include <fmt/format.h>

#include "terminal.hpp"

template<class... Args_>
void Error(const std::string &fmtStr, Args_ &&...args) {
    if (logLevel >= LogLevel::Error) {
        std::cout << red << "[ERROR]" << reset << Format(fmtStr, std::forward<Args_>(args)...);
    }
}
