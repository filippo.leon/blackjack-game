#pragma once

#include "config/config.hpp"

#if B21_TRACING_TYPE_EQUAL(B21_TRACING_TYPE_TRACY)
#    include "Tracy.hpp"

#    define B21_SET_THREAD_NAME(threadName) tracy::SetThreadName(threadName)
#    define B21_TRACE_SCOPE() ZoneScoped
#    define B21_NAMED_TRACE_SCOPE(name) ZoneScopedN((name))
#    define B21_IDLE_SCOPE() ZoneScopedC(0xFFFF00)
#    define B21_NAMED_IDLE_SCOPE(name) ZoneScopedNC((name), 0xFFFF00)
#    define B21_DYNAMIC_SCOPE(name, dynName)                                                                           \
        ZoneScopedN(name);                                                                                             \
        ZoneText((dynName).data(), (dynName).size())

#    define B21_FRAME_START() FrameMark
#    define B21_NAMED_FRAME_START(name) FrameMarkNamed((name))

#    define B21_MARK_ALLOC(ptr, size) TracyAlloc(ptr, size)
#    define B21_MARK_FREE(ptr) TracyFree(ptr)
#else
#    define B21_SET_THREAD_NAME(threadName)
#    define B21_TRACE_SCOPE()
#    define B21_NAMED_TRACE_SCOPE(name)
#    define B21_IDLE_SCOPE()
#    define B21_NAMED_IDLE_SCOPE(name)
#    define B21_DYNAMIC_SCOPE(name, dynName)

#    define B21_FRAME_START()
#    define B21_NAMED_FRAME_START(name)

#    define B21_MARK_ALLOC(ptr, size)
#    define B21_MARK_FREE(ptr)

#endif
