#pragma once

#include <cassert>
#include <iostream>
#include <memory>

#include <conio.h>
#include <ctype.h>

#include "rules.hpp"
#include "strategy/IPlayerStrategy.hpp"
#include "util/debug.hpp"
#include "util/format.hpp"

struct RuleSet;
// TODO: add interactive

class InteractivePlayerStrategy : public IPlayerStrategy {
public:
    int GetBet() const override {
        Output<0>("Choose bet:");
        int n = 10;
        std::cin >> n;
        return n;
    }

    char GetChar() const { return static_cast<char>(toupper(_getch())); }

    Action Decide(const Hand & /*dealerHand*/, const Hand & /*playerHand*/, int /*numHands*/,
                  const PossibleActions &possibleActions) const override {
        const bool hittable = !possibleActions.onlySurrenderable;
        const bool stayable = !possibleActions.onlySurrenderable;
        const bool surrenderable = possibleActions.onlySurrenderable;
        const bool doubleable = possibleActions.doubleable && !possibleActions.onlySurrenderable;
        const bool splittable = possibleActions.splittable && !possibleActions.onlySurrenderable;
        while (true) {
            std::string str = "Choose action (";
            if (hittable || stayable) {
                str += "[H]it, [S]tay";
            }
            if (surrenderable) {
                str += "[C]ontinue, Su[R]render";
            }
            if (doubleable) {
                str += ", [D]ouble";
            }
            if (splittable) {
                str += ", S[P]lit";
            }
            str += ", [Q]uit):\n";
            Output<0>(str);

            char c = GetChar();
            if (hittable && c == 'H') {
                return Action::Hit;
            }
            if (stayable && c == 'S') {
                return Action::Stay;
            }
            if (surrenderable && c == 'R') {
                return Action::Surrender;
            }
            if (surrenderable && c == 'C') {
                return Action::Stay; // Continue
            }
            if (doubleable && c == 'D') {
                return Action::Double;
            }
            if (splittable && c == 'P') {
                return Action::Split;
            }
            if (c == 'Q') {
                std::exit(0); // TODO: are you sure
            }
        }
        ASSERT(false && "Invalid decision");
        return Action::Hit; // Invalid
    }
};
