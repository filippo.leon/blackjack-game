#pragma once

#include "types.hpp"

struct PossibleActions;

class IPlayerStrategy {
public:
    virtual int GetBet() const = 0;

    virtual Action Decide(const Hand &dealerHand, const Hand &playerHand, int numHands,
                          const PossibleActions &possibleActions) const = 0;

    virtual void Finalize() const {}
};
