#pragma once

#include <array>

#include "rules.hpp"
#include "strategy/IPlayerStrategy.hpp"
#include "util.hpp"

// clang-format off
static inline const std::array<std::array<Action, 10>, 17> normalTable = {
        //            A,      2           3           4           5           6          7          8           9           10
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,       Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 5
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,       Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 6
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,       Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 7
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,       Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 8
        std::array{   Action::Hit,    Action::Hit,        Action::Double,     Action::Double,     Action::Double,     Action::Double,    Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 9
        std::array{   Action::Hit,    Action::Double,     Action::Double,     Action::Double,     Action::Double,     Action::Double,    Action::Double,    Action::Double,     Action::Double,     Action::Hit, }, // 10
        std::array{   Action::Hit,    Action::Double,     Action::Double,     Action::Double,     Action::Double,     Action::Double,    Action::Double,    Action::Double,     Action::Double,     Action::Double, }, // 11
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Stay,       Action::Stay,       Action::Stay,      Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 12
        std::array{   Action::Hit,    Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 13
        std::array{   Action::Hit,    Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Hit,       Action::Hit,        Action::Hit,        Action::Surrender, }, // 14
        std::array{   Action::Hit,    Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Hit,       Action::Hit,        Action::Hit,        Action::Surrender, }, // 15
        std::array{   Action::Hit,    Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Hit,       Action::Hit,        Action::Surrender,  Action::Surrender, }, // 16
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 17
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 18
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 19
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 20
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 21
};
static inline const std::array<std::array<Action, 10>, 10> softTable = {
        //            A,      2           3           4           5           6          7          8           9           10
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,        Action::Hit,       Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 12 // two aces and cannot Action::Split
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Double,     Action::Double,    Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 13
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Double,     Action::Double,    Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 14
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Double,     Action::Double,     Action::Double,    Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 15
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Double,     Action::Double,     Action::Double,    Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 16
        std::array{   Action::Hit,    Action::Hit,        Action::Double,     Action::Double,     Action::Double,     Action::Double,    Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 17
        std::array{   Action::Hit,    Action::Stay,       Action::Double,     Action::Double,     Action::Double,     Action::Double,    Action::Stay,      Action::Stay,       Action::Hit,        Action::Hit, }, // 18
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 19
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 20
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 21
};
static inline const std::array<std::array<Action, 10>, 10> splitTable = {
        //            A,      2           3           4           5           6          7          8           9           10
        std::array{   Action::Split,  Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Split,     Action::Split,      Action::Split,      Action::Split, }, // A-A
        std::array{   Action::Hit,    Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Split,     Action::Hit,        Action::Hit,        Action::Hit, }, // 2-2
        std::array{   Action::Hit,    Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Split,     Action::Hit,        Action::Hit,        Action::Hit, }, // 3-3
        std::array{   Action::Hit,    Action::Hit,        Action::Hit,        Action::Hit,        Action::Split,      Action::Split,     Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 4-4
        std::array{   Action::Hit,    Action::Double,     Action::Double,     Action::Double,     Action::Double,     Action::Double,    Action::Double,    Action::Double,     Action::Double,     Action::Hit, }, // 5-5
        std::array{   Action::Hit,    Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Hit,       Action::Hit,        Action::Hit,        Action::Hit, }, // 6-6
        std::array{   Action::Hit,    Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Split,     Action::Hit,        Action::Hit,        Action::Surrender, }, // 7-7
        std::array{   Action::Split,  Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Split,     Action::Split,      Action::Split,      Action::Surrender, }, // 8-8
        std::array{   Action::Stay,   Action::Split,      Action::Split,      Action::Split,      Action::Split,      Action::Split,     Action::Stay,      Action::Split,      Action::Split,      Action::Stay, }, // 9-9
        std::array{   Action::Stay,   Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,       Action::Stay,      Action::Stay,      Action::Stay,       Action::Stay,       Action::Stay, }, // 10-10
};
// clang-format on

class OptimalPlayerStrategy : public IPlayerStrategy {
public:
    int GetBet() const override { return 10; }

    Action Decide(const Hand &dealerHand, const Hand &playerHand, int numHands,
                  const PossibleActions &possibleActions) const override {

        bool playerSoft = false;
        const int playerValue = GetValue(playerHand, &playerSoft);
        // const int dealerValue = GetValue(dealerHand); // dealer soft unkown, he has only one card

        const int dealerValue = GetValue(dealerHand[0]);
        if (possibleActions.splittable) {
            const Action action = splitTable[GetValue(playerHand[0]) - 1][dealerValue - 1];

            if (action == Action::Surrender && !possibleActions.onlySurrenderable) {
                return Action::Hit;
            }
            if (action == Action::Double && !possibleActions.doubleable) {
                return Action::Hit;
            }

            // what if cannot split?
            // if (splittable || action != Action::Split) {
            return action;
            // }
        }
        if (playerSoft) {
            const Action action = softTable[playerValue - 12][dealerValue - 1];

            if (action == Action::Double && !possibleActions.doubleable) {
                return playerValue == 18 ? Action::Stay : Action::Hit;
            }

            return action;
        }

        Action action =
                normalTable[std::max(playerValue - 5, 0)][dealerValue - 1]; // Can happen that you wanna split but cant
        if (action == Action::Double && !possibleActions.doubleable) {
            return Action::Hit;
        }
        if (action == Action::Surrender && !possibleActions.onlySurrenderable) {
            return Action::Hit;
        }
        return action;
    }
};
