#pragma once

#include <iostream>

#include "strategy/IPlayerStrategy.hpp"
#include "strategy/InteractivePlayerStrategy.hpp"
#include "strategy/OptimalPlayerStrategy.hpp"
#include "types.hpp"
#include "util/terminal.hpp"

class TrainingPlayerStrategy : public IPlayerStrategy {
public:
    int GetBet() const override {
        return m_playerStrategy.GetBet();
        // return m_interactivePlayerStrategy.GetBet();
    }

    Action Decide(const Hand &dealerHand, const Hand &playerHand, int numHands,
                  const PossibleActions &possibleActions) const override {
        Action suggestedAction = m_playerStrategy.Decide(dealerHand, playerHand, numHands, possibleActions);
        Action myAction = m_interactivePlayerStrategy.Decide(dealerHand, playerHand, numHands, possibleActions);
        if (possibleActions.onlySurrenderable) {
            if (suggestedAction == Action::Surrender && myAction != Action::Surrender) {
                Output<0>("{}Error:{} you should have surrendered!\n", red, reset);
            } else if (suggestedAction != Action::Surrender && myAction == Action::Surrender) {
                Output<0>("{}Error:{} you should have continued!\n", red, reset);
            } else {
                Output<0>("{}Correct!!!{}\n", green, reset);
            }
        } else {
            if (suggestedAction != myAction) {
                Output<0>("{}Error:{} you should have chosen {}!\n", red, reset, suggestedAction);
            } else {
                Output<0>("{}Correct!!!{}\n", green, reset);
            }
        }
        return myAction;
    }

    void Finalize() const override {
        Output<0>("Press Enter to Continue...\n");
        std::cin.ignore();
    }

private:
    OptimalPlayerStrategy m_playerStrategy;
    InteractivePlayerStrategy m_interactivePlayerStrategy;
};
