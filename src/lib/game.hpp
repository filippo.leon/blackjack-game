#pragma once

class Dealer;
class Player;
struct RuleSet;

void PlayRound(Dealer &dealer, Player &player, const RuleSet &ruleSet);
