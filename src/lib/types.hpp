#pragma once

#include <vector>

constexpr int BlackJackValue = 21;

enum class Card { Invalid = 0, A = 1, First = A, N2, N3, N4, N5, N6, N7, N8, N9, N10, J, Q, K, Last = K };

using Hand = std::vector<Card>;

enum class Action { Invalid, Stay, Hit, Split, Double, Surrender };

enum class Status { Invalid, Win, Loss, Tie, Surrendered, BlackJack };
