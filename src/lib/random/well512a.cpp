#include "random/well512a.hpp"

constexpr unsigned int W = 32;
constexpr unsigned int P = 0;
constexpr unsigned int M1 = 13;
constexpr unsigned int M2 = 9;
constexpr unsigned int M3 = 5;

unsigned int MAT0POS(int t, unsigned int v) {
    return v ^ (v >> t);
}

unsigned int MAT0NEG(int t, unsigned int v) {
    return v ^ (v << (-(t)));
}

unsigned int MAT3NEG(int t, unsigned int v) {
    return v << (-(t));
}

unsigned int MAT4NEG(int t, unsigned int b, unsigned int v) {
    return v ^ ((v << (-(t))) & b);
}

unsigned int V0(unsigned int s) {
    return s;
}
unsigned int VM1(unsigned int s) {
    return (s + M1) & 0x0000000fU;
}
unsigned int VM2(unsigned int s) {
    return (s + M2) & 0x0000000fU;
}
unsigned int VM3(unsigned int s) {
    return (s + M3) & 0x0000000fU;
}
unsigned int VRm1(unsigned int s) {
    return (s + 15) & 0x0000000fU;
}
unsigned int VRm2(unsigned int s) {
    return (s + 14) & 0x0000000fU;
}
unsigned int newV0(unsigned int s) {
    return (s + 15) & 0x0000000fU;
}
unsigned int newV1(unsigned int s) {
    return s;
}
unsigned int newVRm1(unsigned int s) {
    return (s + 14) & 0x0000000fU;
}

Well512a::Well512a(const std::array<unsigned int, Well512a::StateSize> &init) {
    m_state_i = 0;
    for (int j = 0; j < Well512a::StateSize; j++) {
        m_state[j] = init[j];
    }
}

unsigned int Well512a::operator()() {
    m_z0 = m_state[VRm1(m_state_i)];
    m_z1 = MAT0NEG(-16, m_state[V0(m_state_i)]) ^ MAT0NEG(-15, m_state[VM1(m_state_i)]);
    m_z2 = MAT0POS(11, m_state[VM2(m_state_i)]);
    m_state[newV1(m_state_i)] = m_z1 ^ m_z2;
    m_state[newV0(m_state_i)] = MAT0NEG(-2, m_z0) ^ MAT0NEG(-18, m_z1) ^ MAT3NEG(-28, m_z2) ^
                                MAT4NEG(-5, 0xda442d24U, m_state[newV1(m_state_i)]);
    m_state_i = (m_state_i + 15) & 0x0000000fU;
    return m_state[m_state_i];
}
