#pragma once

#include <array>
#include <limits>

class Well512a {
public:
    using result_type = unsigned int;

    static constexpr int StateSize = 16;

    Well512a() {}

    explicit Well512a(const std::array<unsigned int, StateSize> &init);

    unsigned int operator()();

    static constexpr int min() noexcept { return 0; }
    static constexpr int max() noexcept { return std::numeric_limits<unsigned int>::max(); }

private:
    unsigned int m_state_i = 0;

    std::array<unsigned int, StateSize> m_state;

    unsigned int m_z0 = 0, m_z1 = 0, m_z2 = 0;
};
