#pragma once

#include "ruleset.hpp"
#include "types.hpp"

struct PossibleActions {
    bool splittable = false;
    bool doubleable = false;
    bool onlySurrenderable = false;
};

inline PossibleActions GetPossibleActions(const RuleSet &ruleSet, const Hand &playerHand, int numHands, bool initial) {
    PossibleActions p;
    if (initial) {
        // TODO: early surrender against ace
        p.onlySurrenderable = true;
    }

    p.splittable = numHands < 4 && (numHands == 1 || playerHand[0] != Card::A) && playerHand.size() == 2 &&
                   playerHand[0] == playerHand[1]; // rule: max 4 hands, max once split ace

    p.doubleable = playerHand.size() == 2; // can double on any 2 cards;

    return p;
}
