
#include <type_traits>

#include <gtest/gtest.h>

#include "deck/RiggedDeck.hpp"

template<class T_>
class Enum {
public:
    static inline constexpr T_ Last = T_::Last;

    using UnderlyingType = std::underlying_type_t<T_>;

    Enum(T_ t) noexcept : m_t(t) {}                          // NOLINT
    Enum(const Enum<T_> &other) noexcept : m_t(other.m_t) {} // NOLINT

    std::strong_ordering operator<=>(const Enum<T_> &other) const = default;
    Enum<T_> operator==(const T_ &other) const { return m_t == other; }

    Enum<T_> &operator++() {
        m_t = static_cast<T_>(static_cast<UnderlyingType>(m_t) + 1);
        return *this;
    }
    Enum<T_> operator++(int) {
        Enum<T_> temp = *this;
        ++*this;
        return temp;
    }
    Enum<T_> &operator--() {
        m_t = static_cast<T_>(static_cast<UnderlyingType>(m_t) - 1);
        return *this;
    }
    Enum<T_> operator--(int) {
        Enum<T_> temp = *this;
        --*this;
        return temp;
    }

private:
    T_ m_t;
};

TEST(Deck, Test1) {
    const int numDecks = 4;
    Deck deck(numDecks);

    std::vector<Card> deckImpl;
    for (int i = 0; i < numDecks * 4 * 13; ++i) {
        deckImpl.push_back(deck.Draw());
    }
    for (Enum<Card> c = Card::A; c < Enum<Card>::Last; ++c) {
        const int count = static_cast<int>(std::count(deckImpl.begin(), deckImpl.end(), Card::A));
        EXPECT_EQ(count, 4 * numDecks);
    }
}

TEST(RiggedDeck, Test1) {
    const int numDecks = 3;
    auto begin = std::vector<Card>{Card::A, Card::K};
    RiggedDeck deck(numDecks, begin);

    std::vector<Card> deckImpl;
    for (int i = 0; i < numDecks * 4 * 13; ++i) {
        deckImpl.push_back(deck.Draw());
    }
    for (int i = 0; i < begin.size(); ++i) {
        EXPECT_EQ(deckImpl[i], begin[i]);
    }
    for (Enum<Card> c = Card::A; c < Enum<Card>::Last; ++c) {
        const int count = static_cast<int>(std::count(deckImpl.begin(), deckImpl.end(), Card::A));
        EXPECT_EQ(count, 4 * numDecks);
    }
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
